import { useEffect, useRef, useState } from 'react';
import './App.css';


function App() {
  var [data,setdata]=useState([]);
  var [page,setPage]=useState(1);
  var [length,setlength] = useState(5);
  var [dup,setdup] = useState([]);
  var [called,setcalled] = useState(false);
  var [popup,setpopup] = useState(false);
  var [popupcontent,setcontent]=useState([]);
  var refer = useRef('');

  useEffect(()=>{
    fetch('https://jsonplaceholder.typicode.com/posts').then((res)=>res.json())
    .then((data)=>{
      console.log(data);
      setdata(data);
    })
  },[])

  function changelength(){
    setlength(10);
  }

  function filter(){
    var array=data;
    array = array.filter((book)=>book.title.includes(refer.current.value));
    setdup(array);
    setcalled(true);
    console.log(data)
    console.log(array)
    console.log(dup)
  }

  return (
    <>
    {popup && popupcontent.length!==0 
    ? <div className='pop_up_container'>
        <div className='pop_up_content'>
          <button onClick={()=>setpopup(false)}> X </button>
          <h3>User ID :</h3> <p>{popupcontent.userId}</p>
          <h3>Title : </h3><p>{popupcontent.title}</p>
          <h3>Description :</h3><p>{popupcontent.body}</p>
          {console.log("yes")}
        </div>
      </div>
    
    : <div className='storage'>
      <center>
        <input ref={refer} data-testid="search" className="searchBox" type="text" placeholder='search a book ...'></input>
        <button data-testid="search_btn" className="search_btn" onClick={()=>filter()}>Search</button>
      </center>

      {data.length > 0
      ?<>
      {dup.length === 0 && called === false ? <div className='container'>
          {data.map((book,index)=>{
            if(index < page*length && index >= (page-1)*length){
              return <div className="cover">
              {
              <div className='book' onClick={()=>{setpopup(true);setcontent(book)}}>
                <h3>Title :</h3><p>{book.title}</p>
                <h3>Description :</h3><p>{book.body}</p>
              </div>
              }
            </div>
            }
            else{
              return null
            }
          })}
      </div>
      : <div className='container'>
        {dup.map((book,index)=>{
          if(index < page*length && index >= (page-1)*length){
            return <div className="cover">
            {
            <div className='book' onClick={()=>{setpopup(true);setcontent(book)}}>
              <h3>Title :</h3><p>{book.title}</p>
              <h3>Description :</h3><p>{book.body}</p>
            </div>
            }
          </div>
          }
          else{
            return null
          }
        })}
    </div>
      }
      <center>
        {length === 5 && <button className="showmore" onClick={()=>changelength()}>show more </button>}
        {<div className="btns">
          {data.map((value,index)=>{
            if(data[index*10] || data[(index*10)+1]){
              return <>
                  <button className="btn" onClick={()=>{setlength(5);setPage(index+1)}}>{index+1}</button>
                  </>
            }
            else{
              return null
            }
             
          })}
        </div>
      }
      </center>
      </>
      : null }
    </div>
  }
    </>
  );
  
}

export default App;
