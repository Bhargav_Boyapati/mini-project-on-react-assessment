import { render, screen } from '@testing-library/react';
import App from './App';

test('renders the input text box', () => {
  render(<App />);

  const search = screen.getByTestId("search");
  expect(search).toBeInTheDocument();
});

test('renders the search button', () => {
  render(<App />);

  const search_btn = screen.getByTestId("search_btn");
  expect(search_btn).toBeInTheDocument();
});

