var post_details = {
    posts_init:[],
    dup:[],
    page:1,
    length : 5,
    called : false,
    popupcontent : [],
    dis_data : []
}
export const reducer = (state = post_details,action) =>{
    switch(action.type){
        case 'INIT': return{...state,
                        posts_init:JSON.parse(localStorage.getItem('posts_data_init'))};

        case 'FILTER':return{...state,
                        called:true,
                        dup:JSON.parse(localStorage.getItem('posts_data_init')).filter((post)=>post.title.includes(action.payload))};

        case 'POPUP' : if(action.payload){
            console.log("have")
                           return {...state,popupcontent:action.payload};
                       }
                       else{
                        console.log("emptied")
                           return {...state,popupcontent:[]};
                       }
            
        case 'SET_PAGE' : return {...state,page:action.payload,length:5};

        case 'SET_LENGTH' : return {...state,length:10};

        default : return {...state};
    }
}