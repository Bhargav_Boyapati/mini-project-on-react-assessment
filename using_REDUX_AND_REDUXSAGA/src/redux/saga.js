import axios from 'axios';
import {put, call,all,takeLatest} from 'redux-saga/effects';

function* init(){
    try{
        var posts=yield call(axios.get,'https://jsonplaceholder.typicode.com/posts');
        var array=[];
        for(var i=0;i<posts.data.length;i++){
            array=[...array,posts.data[i]]
        }
        console.log(array)
        var serialize = JSON.stringify(array)
        localStorage.setItem('posts_data_init',serialize);
        yield put({type:'INIT'})
    }
    catch(error){
        console.log(error);
    }   
}

function* filter(data){
    yield put({
        type:'FILTER',
        payload:data.payload
    })
}

function* popup(data){
    yield put({
        type:'POPUP',
        payload:data.payload
    })
}

function* setpage(data){
    yield put({
        type:'SET_PAGE',
        payload:data.payload
    })
}

function* setlength(){
    yield put({
        type:'SET_LENGTH'
    })
}

export function* watcher()
{
    yield all([
        yield takeLatest('Init_Data',init),
        yield takeLatest('filter',filter),
        yield takeLatest('popup',popup),
        yield takeLatest('setpage',setpage),
        yield takeLatest('setlength',setlength)
    ])
}