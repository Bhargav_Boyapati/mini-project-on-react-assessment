export const init_data =()=>
{
    return{
        type:'Init_Data'
    }
}

export const filter =(data)=>
{
    return{
        type:'filter',
        payload:data
    }
}

export const popupfunc =(data)=>
{
    return{
        type:'popup',
        payload:data
    }
}

export const setpage =(data)=>
{
    return{
        type:'setpage',
        payload:data
    }
}

export const setlength =()=>
{
    return{
        type:'setlength'
    }
}

