import './App.css';
import React, { useEffect, useRef} from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { init_data,filter,popupfunc,setpage,setlength} from './redux/actions';

function App({init_data,posts_init,page,length,filter,dup,popupcontent,popupfunc,setlength,setpage,called}) {

  var refer = useRef('');

  useEffect(()=>
  {
    init_data()
  },[])

  return (
    <div className='App'>
      {/* search box */}
      <div className='input_fld'>
        <input type="text" ref={refer} data-testid="search" placeholder="search with title"></input>
        <button className="search_btn" data-testid="search_btn" onClick={()=>{
          filter(refer.current.value);
          }}>Search</button>
      </div>

      {/* Display all the posts info */}
      {(dup.length === 0 && called === false) && 
        <>
          <div className="container">
            { posts_init.length>0 && 
              posts_init.map((post,index)=>{
              if(index < page*length && index >= (page-1)*length){
                return <div className='post' onClick={()=>popupfunc(post)}>
                          <h3>Title :</h3><p>{post.title}</p>
                          <h3>Description :</h3><p>{post.body}</p>
                       </div>
                       
              }
              else{
                return null
              }
            })
            }
          </div>

          {/* Load More Button */}
          {length===5 && <div className="show"> <button className="showmore" onClick={()=>setlength()}>Show More</button> </div>}

          {/* Pagination */}
          <div className="btns">
            {posts_init.map((value,index)=>{
              if(posts_init[index*10]){
                return <>
                    <button className="btn" onClick={()=>setpage(index+1)}>{index+1}</button>
                    </>
              }
              else{
                return null
              }
                
            })}
          </div>
        </>
      }
      

      {/* Display the posts related to search */}
      {(dup.length > 0 && called === true) && 
        <>
          <div className="container">
            { dup.length>0 && 
              dup.map((post,index)=>{
              if(index < page*length && index >= (page-1)*length){
                return <div className='post' onClick={()=>popupfunc(post)}>
                          <h3>Title :</h3><p>{post.title}</p>
                          <h3>Description :</h3><p>{post.body}</p>
                       </div>
                      
              }
              else{
                return null
              }
            })
            }
          </div>

        {/* Load More Button */}
        {length===5 &&  <div className="show"> <button className="showmore" onClick={()=>setlength()}>Load More</button> </div>}

          {/* Pagination */}
          <div className="btns">
            {dup.map((value,index)=>{
              if(dup[index*10]){
                return <>
                    <button className="btn" onClick={()=>setpage(index+1)}>{index+1}</button>
                    </>
              }
              else{
                return null
              }
                
            })}
          </div>
        </>
      }

      {/* If no posts Found with the search */}
      {(dup.length === 0 && called === true) &&
        <div className='not_found'>Data Not Found !!</div>
      }

      {/* popup */}
      {popupcontent.length !==0 && <div className='pop_up_container'>
          <div className='pop_up_content'>
            <button onClick={()=>popupfunc()}>X</button>
            <h3>User ID :</h3> <p>{popupcontent.userId}</p>
            <h3>ID :</h3> <p>{popupcontent.id}</p>
            <h3>Title :</h3><p>{popupcontent.title}</p>
            <h3>Description :</h3><p>{popupcontent.body}</p>
          </div>
        </div>}

      
    </div>
  );
}

const mapStateToProps = (state) =>{
  return{
    posts_init:state.posts_init,
    dup:state.dup,
    page : state.page,
    length : state.length,
    called : state.called,
    popupcontent : state.popupcontent
  }
}
const mapDispatchToProps = (dispatch) =>{
  return bindActionCreators({
    init_data,
    filter,
    popupfunc,
    setpage,
    setlength
  },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(App)
